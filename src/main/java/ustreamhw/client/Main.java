package ustreamhw.client;

public class Main {
    public static void main(String[] args) {
        String urlEnv = System.getenv("SERVICE_URL");
        String serviceUrl = (urlEnv == null || urlEnv.equals("")) ? "http://localhost:4567/api/time" : urlEnv;

        TimeClient client = new TimeClient(serviceUrl, 5000);
        client.run();
    }
}