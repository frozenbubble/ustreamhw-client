package ustreamhw.client;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import ustreamhw.protos.TimeServiceProtos.TimeResponse;

public class TimeClient {
    private static final Logger log = Logger.getLogger(TimeClient.class.getName());

    private final String serviceUrl;
    private long millis;
    private ScheduledExecutorService sched;
    private UUID id;

    public TimeClient(String serviceUrl, long millis) {
        id = UUID.randomUUID();
        this.serviceUrl = serviceUrl + "/" + id.toString();
        this.millis = millis;
        sched = Executors.newScheduledThreadPool(1);
    }

    public void run() {
        sched.scheduleAtFixedRate(this::callService, 0, millis, TimeUnit.MILLISECONDS);
    }

    private void callService() {
        log.info("Calling service at " + serviceUrl);

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet req = new HttpGet(serviceUrl);

        try {
            HttpResponse resp = client.execute(req);
            byte[] content = EntityUtils.toByteArray(resp.getEntity());
            StatusLine status = resp.getStatusLine();
            TimeResponse time = TimeResponse.newBuilder().mergeFrom(content).build();

            log.info("Response: " + status + "\nTime: " + time.getTime());
        } catch (IOException e) {
            log.severe("Error while trying to call the service. Reason: " + e.getMessage());
        } catch (Exception e) {
            log.severe("Error while processing response. Reason: " + e.getMessage());
        }
    }
}