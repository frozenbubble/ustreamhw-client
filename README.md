This repository contains the code for the server in this assignment.

# Build

### Requirements

This project requires jdk version 1.8 and gradle version 3.x to build

### Steps

To build the project simply cd into the project root directory and hit `gradle build`

### Docker image
Once the build is complete, to build a docker image from the project hit
`docker image build -t ustreamhw-client:latest .`

# Running the application
When running the image you'll need to specify the url to access the TimeService through the `SERVICE_URL` environment variable. This defaults to http://localhost:4567/api/time .
Note that `SERVICE_URL` has to end with '/api/time'.
In case you want to host both the server and the client containers on local host, you'll need to run the container with the `--net="host"` option

E.g. `docker container run -d --net="host" ustreamhw-client:latest`

# Usage

The client simply tries to get the time from the TimeService every 5 seconds until the process is terminated. Whether the request was successful or not is logged to the console. 