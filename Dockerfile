FROM ubuntu:16.04

RUN apt-get update && apt-get install -y openjdk-8* && mkdir /app

COPY build /app

CMD java -jar /app/libs/ustreamhw-client.jar